# Accelerated Computer Science Fundamentals Example Code

**About this Course**

This course teaches how to write a program in the C++ language, including how to set up a development environment for writing and debugging C++ code and how to implement data structures and C++ classes. It is the first course in the Accelerated CS Fundamentals specialization


Syllabus:

https://www.coursera.org/specializations/cs-fundamentals

**Week 1: Introduction**

* Intro to C++
* Setting up a development environment

**Week 2: Understanding the C++ Memory Model**

* Pointers
* Stack Memory
* Heap Memory
* Compiling and running a C++ program 

**Week 3: Developing C++ Classes**

* Class constructors
* Copy constructors 
* Copy assignment operator
* Variable storage
* Class destructor

**Week 4: Engineering C++ Software Solutions**

* Template types
* Tower of Hanoi - Introduction
* Tower of Hanoi - Solution 1
* Tower of Hanoi - Solution 2
* Templates and Classes
* Inheritance


